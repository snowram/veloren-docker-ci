echo "Please enter Veloren's Gitlab runner token. It can be found at https://gitlab.com/veloren/veloren/settings/ci_cd"
read -p "Token: " GITLAB_RUNNER_TOKEN

echo "\nWhat should this runner be called?"
read -p "Name: " GITLAB_RUNNER_NAME

# Start the runner
 docker run -d --name --security-opt seccomp=unconfined "$GITLAB_RUNNER_NAME" --restart always \
  -v /srv/"$GITLAB_RUNNER_NAME"/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

# Register the runner
docker run --rm -t -i \
  -v /srv/"$GITLAB_RUNNER_NAME"/config:/etc/gitlab-runner \
  -v "$(pwd)"/template.toml:/srv/template.toml \
  gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image registry.gitlab.com/veloren/veloren-docker-ci \
  --url "https://gitlab.com/" \
  --registration-token "$GITLAB_RUNNER_TOKEN" \
  --description "$GITLAB_RUNNER_NAME" \
  --tag-list "veloren-docker" \
  --run-untagged="true" \
  --locked="false" \
  --template-config /srv/template.toml
