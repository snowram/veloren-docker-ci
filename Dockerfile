FROM ubuntu:latest

ARG RUST_TOOLCHAIN=nightly-2019-11-04
ARG PROJECTNAME=veloren

RUN apt-get update; export DEBIAN_FRONTEND=noninteractive; \
    apt-get install -y --no-install-recommends --assume-yes \
        gcc \
        binutils \
        libc-dev \
        ca-certificates \
        curl \
        git \
        git-lfs \
        glib2.0 \
        libasound2-dev \
        libatk1.0-dev \
        libcairo2-dev \
        libclang-dev \
        libgtk-3-dev \
        libpango1.0-dev \
        libssl-dev \
        mingw-w64 \
        pkg-config \
        ffmpeg \
        ;

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain ${RUST_TOOLCHAIN}; \
    echo "[target.x86_64-pc-windows-gnu]\nlinker = \"/usr/bin/x86_64-w64-mingw32-gcc\"" >> /root/.cargo/config

RUN . /root/.cargo/env; \
    rustup target add x86_64-pc-windows-gnu; \
    rustup component add rustfmt-preview --toolchain=${RUST_TOOLCHAIN}; \
    rustup component add clippy-preview --toolchain=${RUST_TOOLCHAIN}; \
    RUSTFLAGS="--cfg procmacro2_semver_exempt" cargo install cargo-tarpaulin; \
    cargo install cargo-audit; \
    rustup default ${RUST_TOOLCHAIN}

RUN mkdir -p /dockercache; \
    cd /dockercache; \
    git clone "https://gitlab.com/veloren/${PROJECTNAME}.git"; \
    cd "/dockercache/${PROJECTNAME}"; \
    git lfs install; \
    git lfs fetch --recent; \
    git checkout master; \
    git lfs pull; \
    . /root/.cargo/env; \
    cargo check; \
    cargo clippy -- --warn clippy::all; \
    cargo audit; \
    cargo tarpaulin -v; \
    cargo fmt --all -- --check; \
    cargo test; \
    cargo bench; \
    cargo build; \
    VELOREN_ASSETS=assets cargo build --release; \
    VELOREN_ASSETS=assets cargo build --target=x86_64-pc-windows-gnu --release;
