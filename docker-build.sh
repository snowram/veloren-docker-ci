#!/bin/bash
#make sure do have access to docker, if not set correct rights or use root user
docker login registry.gitlab.com/veloren/veloren-docker-ci;
docker build -t registry.gitlab.com/veloren/veloren-docker-ci:latest --no-cache .;
docker push registry.gitlab.com/veloren/veloren-docker-ci;
