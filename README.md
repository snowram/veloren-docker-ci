# Veloren Docker CI

## Installing a Veloren CI Runner via docker

This holds the Dockerfile that is used to build the CI for Veloren. It also has a script that will set up a runner on a Linux machine. These are the requirements for a runner:

- 50gb free hard drive space
- At least 2 cores or VCPUs
- 3gb avaiable ram
- An unlimited internet connection, as downloads and uploads are done for each run

On top of these requirements, you must get approval from one of the admins of Veloren. This is because we don't want to give the runner token out to everyone, as that could pose a security risk. Ping @AngelOnFira or @xMAC94x for more information.

Once you have the token, clone the repo and run the launch script.

```
git clone https://gitlab.com/veloren/veloren-docker-ci.git
cd veloren-docker-ci/runner
./launch-runner.sh
```

## Make changes to the runner image:
```
git clone https://gitlab.com/veloren/veloren-docker-ci.git
cd veloren-docker-ci
./docker-build.sh
```

